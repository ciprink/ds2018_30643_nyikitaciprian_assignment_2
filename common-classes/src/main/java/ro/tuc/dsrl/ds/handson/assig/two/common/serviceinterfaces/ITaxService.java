package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ITaxService extends Remote {

	double computeTax(Car car) throws RemoteException;
    double computePrice(Car car)throws RemoteException;

}
