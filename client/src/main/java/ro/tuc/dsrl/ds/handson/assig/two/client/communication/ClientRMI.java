package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;

import javax.swing.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientRMI {

    public static void main(String[] args) {

        JFrame frame = new JFrame("Compute Tax");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.getContentPane().setLayout(null);

        JLabel yearLabel = new JLabel("Year:");
        frame.getContentPane().add(yearLabel);

        JLabel engineCapacityLabel = new JLabel("Engine capacity:");
        frame.getContentPane().add(engineCapacityLabel);

        JLabel priceLabel = new JLabel("Price:");
        frame.getContentPane().add(priceLabel);

        JLabel textLabel = new JLabel("");
        frame.getContentPane().add(textLabel);

        JTextArea yearTextArea = new JTextArea();
        frame.getContentPane().add(yearTextArea);

        JTextArea engineCapacityTextArea = new JTextArea();
        frame.getContentPane().add(engineCapacityTextArea);

        JTextArea priceTextArea = new JTextArea();
        frame.getContentPane().add(priceTextArea);

        JButton computeTaxButton = new JButton("Compute tax");
        frame.getContentPane().add(computeTaxButton);

        JButton sellingPriceButton = new JButton("Compute selling price");
        frame.getContentPane().add(sellingPriceButton);

        yearLabel.setBounds(20, 20, 150, 30);
        yearTextArea.setBounds(20, 50, 150, 30);

        engineCapacityLabel.setBounds(20, 80, 150, 30);
        engineCapacityTextArea.setBounds(20, 110, 150, 30);

        priceLabel.setBounds(20, 140, 150, 30);
        priceTextArea.setBounds(20, 170, 150, 30);

        textLabel.setBounds(20, 200, 180, 30);
        computeTaxButton.setBounds(20, 230, 250, 30);
        sellingPriceButton.setBounds(20, 260, 250, 30);

        computeTaxButton.addActionListener(e -> {

            String yearString = yearTextArea.getText();
            String engineCapacityString = engineCapacityTextArea.getText();

            int year = Integer.parseInt(yearString);
            int engineCapacity = Integer.parseInt(engineCapacityString);

            if (year != 0 && engineCapacity != 0) {
                try {

                    Registry registry = LocateRegistry.getRegistry(1888);
                    ITaxService stub = (ITaxService) registry.lookup("ITaxService");

                    textLabel.setText("Tax value: "
                            + stub.computeTax(new Car(year, engineCapacity)));

                } catch (Exception ex) {
                    System.err.println("Client exception: " + ex.toString());
                    ex.printStackTrace();
                }
            }
        });

        sellingPriceButton.addActionListener(e -> {
            String yearString = yearTextArea.getText();
            String engineCapacityString = engineCapacityTextArea.getText();
            String priceString = priceTextArea.getText();

            int year = Integer.parseInt(yearString);
            int engineCapacity = Integer.parseInt(engineCapacityString);
            double price = Double.parseDouble(priceString);

            if (year != 0 && engineCapacity != 0 && price != 0) {
                try {

                    Registry registry = LocateRegistry.getRegistry(1888);
                    ITaxService stub = (ITaxService) registry.lookup("ITaxService");

                    textLabel.setText("Selling price: "
                            + stub.computePrice(new Car(year, engineCapacity, price)));

                } catch (Exception ex) {
                    System.err.println("Client exception: " + ex.toString());
                    ex.printStackTrace();
                }
            }
        });

        frame.setVisible(true);

    }
}
