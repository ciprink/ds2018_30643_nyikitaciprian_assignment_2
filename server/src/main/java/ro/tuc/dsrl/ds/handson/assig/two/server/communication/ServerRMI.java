package ro.tuc.dsrl.ds.handson.assig.two.server.communication;

import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;
import ro.tuc.dsrl.ds.handson.assig.two.server.services.TaxService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerRMI extends TaxService {


    public ServerRMI(){

    }

    public static void main(String[] args) throws Exception {

        TaxService obj = new TaxService();

        ITaxService stub = (ITaxService) UnicastRemoteObject.exportObject(obj,0 );

        Registry registry = LocateRegistry.createRegistry(1888);
        registry.bind("ITaxService",stub );

        System.out.println("Server is running.");
    }
}
