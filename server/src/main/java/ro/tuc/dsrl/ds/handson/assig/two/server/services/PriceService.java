package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

public class PriceService implements IPriceService {

    @Override
    public double computePrice(Car car){
        if (2018 - car.getYear() < 7) {
            return car.getPrice() - car.getPrice() / 7 * (2018 - car.getYear());
        } else {
            return 0;
        }
    }
}
